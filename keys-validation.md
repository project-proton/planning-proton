# Validation Classes
For convenience, we might use the following short words to indicate a kind of validation to avoid long sentences in tables.
#### password
A string of 6-30 charecters.

#### joi uri
Validated by hapi joi's string().uri() method.
# Validation of Keys
The server should throw an error when it cannot validate the keys given in ~/config/keys.js <br>
This document describes the validations relating to keys. <br>

| Key Reference | Description | Validation |
|--|--|--|
| NODE_ENV | | Allowed values - 'development', 'test', 'production' |
| PORT | The port on which to deploy server | Integer in 0 to 65535 |
| mongodb.MONGO_URI | Mongo Database address | joi uri |
| mongodb.MONGO_TEST_URI | Mongo test database address | joi uri |
| passport.JWT_USER_SECRET | Signing secret of jwt user authentication token | password |
| passport.JWT_ADMIN_SECRET | Signing secret of jwt admin authentication token | password |
| passport.SALT_ROUNDS | Specifies number of salt rounds for bcrypt hashing of passwords | integer in 1-20 |


