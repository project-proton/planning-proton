# Documentations

This repository contains documentation of proton.

### Table Of Contents
- [Authentication Requests](./api-docs/authentication-requests.md)
  - Register Participant
  - Get Participant Token
  - Get Admin Token
- [Events Management Requests](./api-docs/events-requests.md)
  - Add Event
  - Put Event
  - Get Events
  - Get Event
  - Change Publicity
- [Colleges Management Requests](./api-docs/colleges-requests.md)
  - Add College
  - Get Colleges
- [Database Schemas](./db-schemas.md)
  - Admin Schema
  - Participant Schema
  - College Schema
  - Event Schema