> Table Of Contents
- [Database Schemas](./db-schemas.md)
- [Keys Validation](./keys-validation.md)
- API
  - [Authentication Requests](./api-docs/authentication-requests.md)
  - [Events Management Requests](./api-docs/events-requests.md)
  - [Colleges Management Requests](./api-docs/colleges-requests.md)
  - [Database Schemas](./db-schemas.md)
