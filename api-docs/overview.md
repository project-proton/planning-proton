# Supported requests
## Authentication Requests

| Title                   | Description                                      |
|-------------------------|--------------------------------------------------|
| Register as participant | Any one can register a participant account       |
| Get participant token    | Any participant can login with valid credentials |
| Get admin token        | Any admin can login with valid credentials       |

## Requests on Behalf of Admins
These requests are authenticated and requires a valid admin token issued upon login to make effect.

### Managing list of events

| Title           | Description                                                                                                                 |
|-----------------|-----------------------------------------------------------------------------------------------------------------------------|
| Put Event       | Creates new event or update existing ones                                                                                   |
| Get Events      | List all event's name, id, and type                                                                                         |
| Get Event       | Get all fields of an event                                                                                                  |
| Change Publicity   | Mark an event as published or not published. Publishing requires certain details to be filled beforehand for Eg. Upper limit on number of participants. |

### Managing the list of colleges




